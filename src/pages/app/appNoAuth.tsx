import React, { lazy } from "react"
import { Route } from "react-router-dom"
import { routersNotAuth } from "@routers/index"

const Components = {}

routersNotAuth.forEach(route => {
  Components[route.component] = lazy(() => import(`@pages/${route.component}`))
})

const App = props => {
  return (
    <>
      {routersNotAuth.map((route, index) => (
        <Route
          key={index}
          exact={route.exact}
          path={route.path}
          render={routeProps => {
            const Component = Components[route.component]
            return <Component {...props} {...routeProps} route={route} />
          }}
        />
      ))}
    </>
  )
}

export default App
