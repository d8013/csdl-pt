import { axiosClient } from "@/config"
import { Button, Col, Row } from "antd"
import { useRef } from "react"
import FormAdmission, { FormAdmissionRef } from "./admission"
import TableAdmision, { PropsTableRef } from "./table"
// import styles from "./index.module.css";

const Home = () => {
  const refForm = useRef<FormAdmissionRef>(null)
  const refTable = useRef<PropsTableRef>(null)

  const onClickPatient = _id => {
    axiosClient.get(`/admission/${_id}`).then(res => {
      refForm?.current?.setFormValue(res?.data)
    })
  }

  const onSave = () => {
      refTable?.current?.reload()
  }

  return (
    <Row style={{ height: "100%" }} gutter={[20, 0]}>
      <Col span={10}>
        <h4>Danh sách tiếp nhận</h4>
        <TableAdmision onClickPatient={onClickPatient} ref={refTable} />
      </Col>
      <Col span={14}>
        <FormAdmission ref={refForm} onCancel={() => refTable?.current?.cancelSelect()} reload={onSave} />
      </Col>
    </Row>
  )
}

export default Home
