import React, { createContext, useReducer } from "react"
import { IAuth, TAuthAction, TAuthContext } from "@common/index"
import { authReducer, FCAuthReducer, initAuthState } from "./auth"

export const AuthContext = createContext<TAuthContext>({
  ...initAuthState,
  dispatchAuth: () => {}
})

export const AuthContextProvider = ({ children }) => {
  const [stateAuth, dispatchAuth] = useReducer<
    FCAuthReducer<IAuth, TAuthAction>
  >(authReducer, initAuthState)

  return (
    <AuthContext.Provider
      value={{
        ...stateAuth,
        dispatchAuth
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}
