import React, { memo } from "react"
// import "./style.css"
import styles from "./style.module.css"
interface GridComponentProps {
  onClick?: () => void
}

const GridComponent: React.FC<GridComponentProps> = memo(({ onClick }) => {
  return (
    <div className={styles["grid-container"]}>
      <p>GridComponent</p>
    </div>
  )
})

export { GridComponent }
